﻿using System;
using NUnit.Framework;

namespace FigureArea
{
    [TestFixture]
    class Test
    {
        [Test]
        public void TestCircleMaxVal()
        {
            //максимальное значение
            double maxRadius = 7.56454557228262E+152;
            double maxArea = 1.7976931348623169E+306;
            double maxCircle = new Circle(maxRadius).Area();

            Assert.That(maxCircle, Is.EqualTo(maxArea));
        }

        [Test]
        public void TestCircleMinVal()
        {
            //минимальное значение
            double minRAdius = 4.94065645841247E-160;
            double minArea = 7.66873873505408E-319;
            double minCircle = new Circle(minRAdius).Area();

            Assert.That(minCircle, Is.EqualTo(minArea));
        }

        [Test]
        public void TestTriangleMaxVal()
        {
            //максимальное значение
            double maxA = 7.56454557228262E+50;
            double maxArea = 2.4778004258774479E+101;
            double maxTriangle = new Triangle(maxA, maxA, maxA).Area();

            Assert.That(maxTriangle, Is.EqualTo(maxArea));
        }

        [Test]
        public void TestTriangleMinVal()
        {
            //минимальное значение
            double minA = 4.94065645841247E-50;
            double minArea = 1.0569877396227369E-99;
            double minTriangle = new Triangle(minA, minA, minA).Area();

            Assert.That(minTriangle, Is.EqualTo(minArea));
        }

        [Test]
        public void TestTriangleNewFormula()
        {
            //другая формула
            double a = 5;
            double areaNewFormula = ((Math.Pow(a, 2.0)) * Math.Sqrt(3)) / 4; ;
            double areaOldFormula = new Triangle(a, a, a).Area();

            Assert.That(areaOldFormula, Is.EqualTo(areaNewFormula));
        }
    }
}
