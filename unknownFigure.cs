﻿namespace FigureArea
{
    public class UnknownFigure 
    {
        IFigure uFigure;
        public UnknownFigure(double a, double b, double c)
        {
            uFigure = new Triangle(a,b,c);
        }

        public UnknownFigure(double radius)
        {
            uFigure = new Circle(radius);
        }

        public double Area()
        {
           return uFigure.Area();
        }
    }
}
