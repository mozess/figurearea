﻿using System;

namespace FigureArea
{
    public class Circle : IFigure
    {
        double radius;
        //конструктор

        public Circle(double radius)
        {
            this.radius = radius;

            if (radius < 0)
                throw new ArgumentException("Радиус не может быть отрицательным числом");
        }
        public double Area()
        {
            return Math.PI * Math.Pow(radius, 2.0);
        }
    }
}
