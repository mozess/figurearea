﻿using System;

namespace FigureArea
{
    public class Triangle : IFigure
    {
        double a;
        double b;
        double c;
        //конструктор
        public Triangle(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            bool rightTriangle = false;

            if (a <= 0 || b <= 0 || c <= 0)
                throw new ArgumentException("Стороны треугольника не могут иметь отрицательное или нулевое значение");
            if (a + b <= a || a + c <= b || b + c <= a)
                throw new ArgumentException("Не верные параметры, используя эти длины сторон не возможно получить треугольник");
            if (Math.Sqrt(a) == Math.Sqrt(b) + Math.Sqrt(c) || Math.Sqrt(b) == Math.Sqrt(a) + Math.Sqrt(c) || Math.Sqrt(c) == Math.Sqrt(a) + Math.Sqrt(b))
                rightTriangle = true;
        }
        public double Area()
        {
            double pp;
            double p;

            pp = (a + b + c) / 2.0;
            p = Math.Sqrt(pp * (pp - a) * (pp - b) * (pp - c));

            return p;
        }
    }
}
